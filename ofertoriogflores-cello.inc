\context Staff = "cello" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Cello"
	\set Staff.shortInstrumentName = "C."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "cello" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "bass"

		\key a \minor
		a 16 a c' a e' c' a a g a c' a e' c' a g  |
		fis 16 a c' a e' c' a fis f a c' a e' c' a f  |
		a 16 a c' a e' c' a a g a c' a e' c' a g  |
		fis 16 a c' a e' c' a fis f a c' a e' c' a f  |
%% 5
		a 16 a c' a e' c' a a g a c' a e' c' a g  |
		fis 16 a c' a e' c' a fis f a c' a e' c' a f  |
		d 2 g 4 a 8 b  |
		c' 2 e' 4 b  |
		a 16 a c' a e' c' a a g a c' a e' c' a g  |
%% 10
		fis 16 a c' a e' c' a fis f a c' a e' c' a f  |
		d 2 g 4 a 8 b  |
		c' 2 e' 4 g  |
		b 1  |
		\key a \major
		a 2 gis  |
%% 15
		fis 2 e  |
		d 1  |
		d 2 e 4 fis 8 gis  |
		a 2 gis  |
		fis 2 e  |
%% 20
		d 2 d 4 cis  |
		b, 2 b, 4 a,  |
		g, 2 g, 4 b,  |
		e 1  |
		a 2 e  |
%% 25
		d 2 e  |
		a 2 e  |
		d 2 e  |
		fis 2 d'  |
		b 8. fis d 8  |
%% 30
		e 2 ~ e  |
		a 2 gis  |
		fis 2 e  |
		a 2 gis  |
		fis 2 e  |
%% 35
		fis 2 d'  |
		b 8. fis d 8 e 8. fis gis 8  |
		a 2 ~ a  |
		\key a \minor
		a 16 a c' a e' c' a a g a c' a e' c' a g  |
		fis 16 a c' a e' c' a fis f a c' a e' c' a f  |
%% 40
		a 16 a c' a e' c' a a g a c' a e' c' a g  |
		fis 16 a c' a e' c' a fis f a c' a e' c' a f  |
		a 16 a c' a e' c' a a g a c' a e' c' a g  |
		fis 16 a c' a e' c' a fis f a c' a e' c' a f  |
		d 2 g 4 a 8 b  |
%% 45
		c' 2 e' 4 b  |
		a 16 a c' a e' c' a a g a c' a e' c' a g  |
		fis 16 a c' a e' c' a fis f a c' a e' c' a f  |
		d 2 g 4 a 8 b  |
		c' 2 e' 4 g  |
%% 50
		b 1  |
		\key a \major
		a 2 gis  |
		fis 2 e  |
		d 1  |
		d 2 e 4 fis 8 gis  |
%% 55
		a 2 gis  |
		fis 2 e  |
		d 2 d 4 cis  |
		b, 2 b, 4 a,  |
		g, 2 g, 4 b,  |
%% 60
		e 1  |
		a 2 e  |
		d 2 e  |
		a 2 e  |
		d 2 e  |
%% 65
		fis 2 d'  |
		b 8. fis d 8  |
		e 2 ~ e  |
		a 2 gis  |
		fis 2 e  |
%% 70
		a 2 gis  |
		fis 2 e  |
		fis 2 d'  |
		b 8. fis d 8 e 8. b, gis, 8  |
		a, 2 ~ a,  |
%% 75
		b 8. fis d 8 e 8. b, gis, 8  |
		a, 2 ~ a,  |
		b 8. fis d 8 e 8. fis gis 8  |
		a 2 ~ a ~  |
		a 2 r  |
		\bar "|."
	}

>>
